﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleApplication2
{
    class Program
    {
        static void Main(string[] args)
        {
            //datos
            double pi = 3.1416, radio = 0, perimetro=0;
            //problema
            Console.WriteLine("introdusca el valor del radio");
            radio = int.Parse(Console.ReadLine());

            perimetro = (pi * 2) * radio;
            Console.WriteLine("su perimetro es " + perimetro);
            Console.ReadLine();
        }
    }
}
